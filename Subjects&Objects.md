Описание ключевых сущностей в проекте - какими типами объектов будут оперировать пользователи системы, какие у них есть свойства и связи.

Классы:

- Interface User:
    - {name}
    - {login details}
    - {id}
    - {rating}
    - {reviewes}
    - Private Class Review:
        - {user}
        - {body}
        > Класс `Отзыв` хранит информацию об отзыве
> Интерфейс пользователя хранит базовую информацию которая есть у каждого пользователя.
- Реализации User:
    1. Class OridinaryUser:
        - {name}
        - {login details}
        - {id}
        - {created quests}
    > Реализация User `Заказчик` поле {created quests} хранит созданные пользователем квесты.
    2. Class Hero:
        - {name}
        - {login details}
        - {id}
        - {active quests}
    > Реализация User `Герой` поле {active quests} хранит принятые пользователем квесты.
    3. Class Chase:
        - {name}
        - {login details}
        - {id}
        - {active quests}
        > Реализация User `Гонец` поле {active quests} хранит принятые на "гонение" пользователем квесты.
- Class Quest:
    - {status}
    - {creator}
    - {hero}
    - {title}
    - {description}
    - {type}
    - {difficulty}
>  Класс `Квест` хранит информацию о квесте

> - {status} стаус квеста (не назначен/выполняется/выполнен)
> - {creator} и {hero} информация об участниках сделки
> - {title} и {description} текстовая информация, задается заказчиком
> - {type} и {difficulty} данные для сортировки
